The following tutorials show examples of modeling according to different conventions. 

[1_BPMN](1_BPMN.md) shows the use of Business Process Modeling Notiation for the example of a design review process.

[2_UML2](2_UML2.md) shows the modeling of a room temperature and humidity control by an occupant using UML or SysML notation.

[3_Domain model](3_Domain model.md) shows the modeling of server infrastructure, fairage broking and IoT components for a monitoring use case, for example to monitor temperature and humidity in rooms.